﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;
using VRageRender;

namespace IngameScript
{
    partial class Program : MyGridProgram 
    {

        List<IMyGyro> gyros = new List<IMyGyro>();
        List<IMyCockpit> cockpits = new List<IMyCockpit>();
        List<IMyThrust> thrusters = new List<IMyThrust>();
        List<IMyCameraBlock> cameras = new List<IMyCameraBlock>();


        IMyCockpit myCock;
        bool TargetLocked = false;


        TorpedoController torpedoController;
        TargetCapturer capturer;

        ulong tickCounter = 0;
        string text = "";

        public Program()
        {
            this.Runtime.UpdateFrequency = UpdateFrequency.Update1;
            this.GridTerminalSystem.GetBlocksOfType(gyros);
            this.GridTerminalSystem.GetBlocksOfType(cockpits);
            this.GridTerminalSystem.GetBlocksOfType(thrusters);
            this.GridTerminalSystem.GetBlocksOfType(cameras);

            this.myCock = cockpits.First();

            this.capturer = new TargetCapturer(this, cameras.First());
            this.torpedoController = new TorpedoController(this, gyros, cockpits.First(), thrusters.First(), 100);
        }

        

        public void Main(string argument, UpdateType updateSource)
        {
            //torpedoController.TargetPosition(new Vector3(-37337.66,-37196.5,-37237.45), this.gyros, myCock);

            if (argument.Contains("LOCK"))
            {
                capturer.StartTargetRouting();
            }
            if (capturer.Locked)
            {
                capturer.UpdateLock();
            }
            Echo(text);
            this.tickCounter++;
        }
    }
}
