﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Noise.Modifiers;
using VRageMath;
using VRageRender;

namespace IngameScript
{
    class TorpedoController
    {
        private const float DoublePI = (float)(2.0f * Math.PI);
        Dictionary<IMyGyro, MatrixD> gyros;
        IMyCockpit cockpit;
        IMyThrust thruster;

        float maxSpeed;

        Program pr;

        public TorpedoController(Program pr, List<IMyGyro> gyros, IMyCockpit cockpit, IMyThrust thruster, float maxSpeed)
        {
            this.pr = pr;
            this.maxSpeed = maxSpeed;
            this.cockpit = cockpit;
            this.thruster = thruster;

            this.gyros = new Dictionary<IMyGyro, MatrixD>();
            gyros.ForEach(p => this.gyros.Add(p, cockpit.WorldMatrix * MatrixD.Transpose(p.WorldMatrix)));

        }
        public void Activate()
        {
            foreach (var p in this.gyros)
            {
                p.Key.GyroOverride = true;
            }
        }

        public void TargetPosition(Vector3D target, List<IMyGyro> gyros, IMyCockpit cockpit)
        {
            var myPos = cockpit.GetPosition();
            var targ = Vector3D.Normalize(target - myPos);
            //Гасим инерцию
            var vels = this.cockpit.GetShipVelocities();

            var SpeedReject = Vector3D.Reject(vels.LinearVelocity, targ);
            //lerp тем больше, чем выше скорость, и больше расстояние

            double speedLerp = SpeedReject.Length() / maxSpeed;
            if (speedLerp > 1) speedLerp = 1;


            targ = Vector3D.Normalize(Vector3D.Lerp(targ, -Vector3D.Normalize(SpeedReject), speedLerp));
            Vector3D rotation = new Vector3D(
                Vector3D.Dot(targ, cockpit.WorldMatrix.Down) * Math.PI,
                Vector3D.Dot(targ, cockpit.WorldMatrix.Right) * Math.PI,
                0
                );
            foreach (var p in this.gyros)
            {
                
                Vector3D localRotation = Vector3D.Rotate(rotation, p.Value);

                p.Key.Pitch = (float)localRotation.X;
                p.Key.Yaw = (float)localRotation.Y;
                p.Key.Roll = (float)localRotation.Z;
            }
            var degree = Math.Acos(cockpit.WorldMatrix.Forward.Dot(targ)) / Math.PI * 360.0f;
            if (degree > 50)
            {
                this.thruster.Enabled = false;
            }
            else
            {
                this.thruster.Enabled = true;
                this.thruster.ThrustOverridePercentage = 1f;
            }

        }
    }
}
