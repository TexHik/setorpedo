﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;
using VRageRender;

namespace IngameScript
{
    class TargetCapturer
    {
        IMyCameraBlock camera;
        Program pr;


        double Distance = 10000;

        MyDetectedEntityInfo target;
        public bool Locked = false;

        public TargetCapturer(Program pr, IMyCameraBlock camera)
        {
            this.camera = camera;
            this.pr = pr;
            this.camera.Enabled = true;
            this.camera.EnableRaycast = true;
        }

        public void StartTargetRouting()
        {
            if (!Locked)
            {
                this.target = this.camera.Raycast(Distance);
                if (!target.IsEmpty())
                {
                    this.Locked = true;
                    
                }
            }
        }

        public void UpdateLock()
        {
            target.TimeStamp.
        }




    }
}
